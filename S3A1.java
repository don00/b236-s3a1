
package com.zuitt.activity;
import java.util.Scanner;

public class S3A1 {
    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);
        int number, answer =1 , counter = 1;
        

        while(true){
            
            System.out.println("Input and Integer whose factorial will be computed: ");
            
            try{
                number = scanner.nextInt();
                answer = computeFactorial(number);
                if(number == 1){
                   System.out.println("The factorial of " + number + " is " + number);                    
                   break;
                }
                System.out.println("The factorial of " + number + " is " + answer);
                break;
            } 
            catch (Exception e) {
                System.out.println("Please enter a number >= 1");
                scanner.next();
            }
            
        }
        
        
        
    }
    
    public static int computeFactorial(int number){
        int counter = 1, answer = 1;
        while(counter <= number){
            answer *= counter++;
        }
        return answer;
    }
}
